package org.bitbucket.parangee.hideandseek.commands

import com.github.noonmaru.tap.command.ArgumentList
import com.github.noonmaru.tap.command.CommandComponent
import org.bitbucket.parangee.hideandseek.plugin.HideAndSeek
import org.bukkit.command.CommandSender

class SetCountingTime : CommandComponent {
    override val argsCount = 1
    private val pl = HideAndSeek.Inst.ance()
    private val config = pl.config
    override fun onCommand(sender: CommandSender, label: String, componentLabel: String, args: ArgumentList): Boolean {
        if (args.hasNext()) {
            val arg = args.next()
            try {
                Integer.parseInt(arg)
                config.set("settings.countTime", Integer.parseInt(arg))
                config.save(pl.configFile)
                sender.sendMessage("변경되었습니다.")
            } catch (e:NumberFormatException) {
                sender.sendMessage("/hs setCountingTime <int>")
                return false
            }
        }
        return true
    }
}
class SetPlayTime : CommandComponent {
    override val argsCount = 1
    private val pl = HideAndSeek.Inst.ance()
    private val config = pl.config
    override fun onCommand(sender: CommandSender, label: String, componentLabel: String, args: ArgumentList): Boolean {
        if (args.hasNext()) {
            val arg = args.next()
            try {
                Integer.parseInt(arg)
                config.set("settings.time", Integer.parseInt(arg))
                config.save(pl.configFile)
                sender.sendMessage("변경되었습니다.")
            } catch (e:NumberFormatException) {
                sender.sendMessage("/hs setPlayTime <int>")
                return false
            }
        }
        return true
    }
}