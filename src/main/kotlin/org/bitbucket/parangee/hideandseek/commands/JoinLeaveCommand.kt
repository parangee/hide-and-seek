package org.bitbucket.parangee.hideandseek.commands

import com.github.noonmaru.tap.command.ArgumentList
import com.github.noonmaru.tap.command.CommandComponent
import org.bitbucket.parangee.chatbuilder.api.ChatBuilder
import org.bitbucket.parangee.chatbuilder.api.ChatGroup
import org.bukkit.Bukkit
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

class JoinCommand : CommandComponent {
    override fun onCommand(sender: CommandSender, label: String, componentLabel: String, args: ArgumentList): Boolean {
        if (TaskInstance.players.contains(sender as Player)) {
            val builder = ChatBuilder("이미 참여함")
            val group = ChatGroup("이미 참여되어 있습니다. /hs leave로 관전할 수 있습니다.")
            builder.add(group)
            sender.sendMessage(builder.getResult())
        } else {
            if (TaskInstance.getInstance().started) {
                TaskInstance.players.add(sender)
                val builder = ChatBuilder("플레이어 참여")
                val group = ChatGroup("${sender.name}님이 게임에 참여했습니다.(참여자 수: ${TaskInstance.players.size})")
                builder.add(group)
                Bukkit.broadcastMessage(builder.getResult())
            } else {
                sender.sendMessage("게임이 시작되지 않았습니다.")
            }
        }
        return true
    }

    override fun test(sender: CommandSender): (() -> String)? {
        if (sender is Player) return null
        return {"콘솔에서 사용할수 없는 명령어입니다."}
    }
}

class LeaveCommand : CommandComponent {
    override fun onCommand(sender: CommandSender, label: String, componentLabel: String, args: ArgumentList): Boolean {
        if (!TaskInstance.players.contains(sender as Player)) {
            val builder = ChatBuilder("참여하지 않았습니다")
            val group = ChatGroup("참여되어있지 않습니다.. /hs join으로 게임에 참여하세요.")
            builder.add(group)
            sender.sendMessage(builder.getResult())
        } else {
            if (TaskInstance.getInstance().started && TaskInstance.getInstance().waiting) {
                TaskInstance.players.remove(sender)
                val builder = ChatBuilder("플레이어 퇴장(?)")
                val group = ChatGroup("${sender.name}님이 관전합니다.(참여자 수: ${TaskInstance.players.size})")
                builder.add(group)
                Bukkit.broadcastMessage(builder.getResult())
            } else {
                sender.sendMessage("시작 대기중이 아닙니다")
            }
        }
        return true
    }
}