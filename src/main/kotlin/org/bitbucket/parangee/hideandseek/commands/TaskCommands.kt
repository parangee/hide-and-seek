package org.bitbucket.parangee.hideandseek.commands

import com.github.noonmaru.tap.command.ArgumentList
import com.github.noonmaru.tap.command.CommandComponent
import org.bitbucket.parangee.hideandseek.util.TaskManager
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

object TaskInstance {
    val players = ArrayList<Player>()
    private val instance = TaskManager()
    fun getInstance() : TaskManager {
        return instance
    }
}

class StartGame() : CommandComponent {
    override fun onCommand(sender: CommandSender, label: String, componentLabel: String, args: ArgumentList): Boolean {
        val res = TaskInstance.getInstance().start()
        if (!res) {
            sender.sendMessage("게임이 진행중입니다.")
        }
        return true
    }

    override fun test(sender: CommandSender): (() -> String)? {
        if (sender.hasPermission("hs.command.start")) {
            return null
        }
        return {"권한이 없습니다."}
    }
}
class StopGame() : CommandComponent {
    override fun onCommand(sender: CommandSender, label: String, componentLabel: String, args: ArgumentList): Boolean {
        val res = TaskInstance.getInstance().stop()
        if (!res) {
            sender.sendMessage("게임이 진행중이지 않습니다.")
        }
        return true
    }
    override fun test(sender: CommandSender): (() -> String)? {
        if (sender.hasPermission("hs.command.stop")) {
            return null
        }
        return {"권한이 없습니다."}
    }
}