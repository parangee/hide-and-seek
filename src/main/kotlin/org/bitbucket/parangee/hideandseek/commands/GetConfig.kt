package org.bitbucket.parangee.hideandseek.commands

import com.github.noonmaru.tap.command.ArgumentList
import com.github.noonmaru.tap.command.CommandComponent
import org.bitbucket.parangee.hideandseek.plugin.HideAndSeek
import org.bitbucket.parangee.hideandseek.util.ShowConfig
import org.bukkit.command.CommandSender

class GetConfig:CommandComponent {
    override fun onCommand(sender: CommandSender, label: String, componentLabel: String, args: ArgumentList): Boolean {
        sender.sendMessage(ShowConfig().show(HideAndSeek.Inst.ance().config))
        return true
    }
}