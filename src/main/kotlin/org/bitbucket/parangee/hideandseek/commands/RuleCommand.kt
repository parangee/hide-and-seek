package org.bitbucket.parangee.hideandseek.commands

import com.github.noonmaru.tap.command.ArgumentList
import com.github.noonmaru.tap.command.CommandComponent
import org.bitbucket.parangee.chatbuilder.api.ChatBuilder
import org.bitbucket.parangee.chatbuilder.api.ChatGroup
import org.bukkit.command.CommandSender


class RuleCommand:CommandComponent {
    override val argsCount = 0
    override fun onCommand(sender: CommandSender, label: String, componentLabel: String, args: ArgumentList): Boolean {
        if (args.hasNext()) {
            val next = args.next()
            if (next.toLowerCase() == "game") {
                RuleGameCommand().onCommand(sender)
            } else if (next.toLowerCase() == "item") {
                RuleItemsCommand().onCommand(sender)
            }
        } else {
            val builder = ChatBuilder("게임 설명")
            var item = ChatGroup("게임 방법")
            item.add("술래는 랜덤으로 지정됩니다.")
            item.add("술래는 신속 1레벨을 지급받습니다.")
            item.add("술래를 정하고 나서 30초동안 카운트다운을 합니다.")
            item.add("카운트다운 동안 술래는 감옥에 갇힙니다.")
            item.add("카운트다운이 끝나면 술래가 풀려납니다.")
            item.add("제한시간 3분동안 도망치세요!")
            builder.add(item)
            item = ChatGroup("아이템 설명")
            item.add("블라인드 활 - 술래에게 맞추면 술래에게 3초동안 실명이 적용됩니다.")
            item.add("사과 - HP를 최대로 채워줍니다.")
            builder.add(item)
            sender.sendMessage(builder.getResult())
        }
        return true
    }

    override fun onTabComplete(
        sender: CommandSender,
        label: String,
        componentLabel: String,
        args: ArgumentList
    ): List<String> {
        val lst = ArrayList<String>()
        lst.add("game")
        lst.add("item")
        return lst
    }
}
private class RuleGameCommand {
    fun onCommand(sender: CommandSender): Boolean {
        val builder = ChatBuilder("게임 설명")
        var item = ChatGroup("게임 방법")
        item.add("술래는 랜덤으로 지정됩니다.")
        item.add("술래는 신속 1레벨을 지급받습니다.")
        item.add("술래를 정하고 나서 30초동안 카운트다운을 합니다.")
        item.add("카운트다운 동안 술래는 감옥에 갇힙니다.")
        item.add("카운트다운이 끝나면 술래가 풀려납니다.")
        item.add("제한시간 3분동안 도망치세요!")
        builder.add(item)
        sender.sendMessage(builder.getResult())
        return true
    }
}
private class RuleItemsCommand {
    fun onCommand(sender: CommandSender): Boolean {
        val builder = ChatBuilder("게임 설명")
        var item = ChatGroup("아이템 설명")
        item.add("블라인드 활 - 술래에게 맞추면 술래에게 3초동안 실명이 적용됩니다.")
        item.add("사과 - HP를 최대로 채워줍니다.")
        builder.add(item)
        sender.sendMessage(builder.getResult())
        return true
    }
}