package org.bitbucket.parangee.hideandseek.commands

import com.github.noonmaru.tap.command.ArgumentList
import com.github.noonmaru.tap.command.CommandComponent
import org.bitbucket.parangee.hideandseek.plugin.HideAndSeek
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

class SetStartLocation : CommandComponent {
    private val pl = HideAndSeek.Inst.ance()
    private val config = pl.config
    override fun onCommand(sender: CommandSender, label: String, componentLabel: String, args: ArgumentList): Boolean {
        val p = sender as Player
        val l = p.location
        config.set("loc.start", "${l.world.name}|${l.x}|${l.y}|${l.z}")
        config.save(pl.configFile)
        p.sendMessage("설정되었습니다.")
        return true
    }

    override fun test(sender: CommandSender): (() -> String)? {
        if (sender.hasPermission("hs.command.loc.start")) {
            if (sender is Player) return null
            return {"플레이어만 사용할수 있습니다."}
        } else {
            return {"권한이 없습니다."}
        }
    }
}
class SetJailLocation : CommandComponent {
    private val pl = HideAndSeek.Inst.ance()
    private val config = pl.config
    override fun onCommand(sender: CommandSender, label: String, componentLabel: String, args: ArgumentList): Boolean {
        val p = sender as Player
        val l = p.location
        config.set("loc.jail", "${l.world.name}|${l.x}|${l.y}|${l.z}")
        config.save(pl.configFile)
        p.sendMessage("설정되었습니다.")
        return true
    }

    override fun test(sender: CommandSender): (() -> String)? {
        if (sender.hasPermission("hs.command.loc.jail")) {
            if (sender is Player) return null
            return {"플레이어만 사용할수 있습니다."}
        } else {
            return {"권한이 없습니다."}
        }
    }
}
class SetSpawnLocation : CommandComponent {
    private val pl = HideAndSeek.Inst.ance()
    private val config = pl.config
    override fun onCommand(sender: CommandSender, label: String, componentLabel: String, args: ArgumentList): Boolean {
        val p = sender as Player
        val l = p.location
        config.set("loc.spawn", "${l.world.name}|${l.x}|${l.y}|${l.z}")
        config.save(pl.configFile)
        p.sendMessage("설정되었습니다.")
        return true
    }

    override fun test(sender: CommandSender): (() -> String)? {
        if (sender.hasPermission("hs.command.loc.spawn")) {
            if (sender is Player) return null
            return {"플레이어만 사용할수 있습니다."}
        } else {
            return {"권한이 없습니다."}
        }
    }
}