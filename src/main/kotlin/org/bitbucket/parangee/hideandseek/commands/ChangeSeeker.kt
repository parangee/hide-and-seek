package org.bitbucket.parangee.hideandseek.commands

import com.github.noonmaru.tap.command.ArgumentList
import com.github.noonmaru.tap.command.CommandComponent
import org.bitbucket.parangee.hideandseek.plugin.HideAndSeek
import org.bukkit.Bukkit
import org.bukkit.command.CommandSender
import java.util.*
import kotlin.collections.ArrayList

private val pl = HideAndSeek.Inst.ance()

class SetSeeker : CommandComponent {
    override val argsCount = 1
    override fun onCommand(sender: CommandSender, label: String, componentLabel: String, args: ArgumentList): Boolean {
        if (args.hasNext()) {
            val arg = args.next()
            if (Bukkit.getPlayer(arg) != null) {
                pl.config.set("status.seeker", Bukkit.getPlayer(arg)?.uniqueId.toString())
                pl.config.save(pl.configFile)
                Bukkit.broadcastMessage("술래가 ${Bukkit.getPlayer(UUID.fromString(pl.config.getString("status.seeker"))!!)!!.name}님으로 설정되었습니다")
            } else {
                sender.sendMessage("플레이어가 온라인이 아닙니다.")
            }
        }
        return true
    }

    override fun onTabComplete(sender: CommandSender, label: String, componentLabel: String, args: ArgumentList): List<String> {
        val list = ArrayList<String>()
        val txt = args.next()
        Bukkit.getOnlinePlayers().forEach {
            if (it.name.toLowerCase().startsWith(txt.toLowerCase())) {
                list.add(it.name)
            }
        }
        return list
    }
    override fun test(sender: CommandSender): (() -> String)? {
        if (sender.hasPermission("hs.command.seeker.reset")) {
            return null
        }
        return {"권한이 없습니다"}
    }
}

class RemoveSeeker : CommandComponent {
    override fun onCommand(sender: CommandSender, label: String, componentLabel: String, args: ArgumentList): Boolean {
        pl.config.set("status.seeker", null)
        Bukkit.broadcastMessage("술래가 초기화 되었습니다.")
        return true
    }

    override fun test(sender: CommandSender): (() -> String)? {
        if (sender.hasPermission("hs.command.seeker.reset")) {
            return null
        }
        return {"권한이 없습니다"}
    }
}