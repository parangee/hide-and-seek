package org.bitbucket.parangee.hideandseek.commands

import com.github.noonmaru.tap.command.ArgumentList
import com.github.noonmaru.tap.command.CommandComponent
import org.bitbucket.parangee.hideandseek.plugin.HideAndSeek
import org.bukkit.command.CommandSender

class EnableItems:CommandComponent {
    override val argsCount = 0
    override fun onCommand(sender: CommandSender, label: String, componentLabel: String, args: ArgumentList): Boolean {
        val inst = HideAndSeek.Inst.ance()
        inst.config.set("status.enabled", true)
        inst.config.save(inst.configFile)
        inst.config.load(inst.configFile)
        sender.sendMessage("활성화 성공")
        return true
    }

    override fun test(sender: CommandSender): (() -> String)? {
        if (sender.hasPermission("hs.command.item.enable")) {
            return null
        }
        return {"권한이 없습니다"}
    }
}
class DisableItems:CommandComponent {
    override val argsCount = 0
    override fun onCommand(sender: CommandSender, label: String, componentLabel: String, args: ArgumentList): Boolean {
        val inst = HideAndSeek.Inst.ance()
        inst.config.set("status.enabled", false)
        inst.config.save(inst.configFile)
        sender.sendMessage("비활성화 성공")
        return true
    }

    override fun test(sender: CommandSender): (() -> String)? {
        if (sender.hasPermission("hs.command.item.disable")) {
            return null
        }
        return {"권한이 없습니다"}
    }
}