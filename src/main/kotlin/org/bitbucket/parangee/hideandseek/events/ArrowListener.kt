package org.bitbucket.parangee.hideandseek.events

import org.bitbucket.parangee.hideandseek.plugin.HideAndSeek
import org.bukkit.Bukkit
import org.bukkit.entity.EntityType
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.ProjectileHitEvent
import org.bukkit.potion.PotionEffect
import org.bukkit.potion.PotionEffectType
import java.util.*

class ArrowListener : Listener {
    private val pl = HideAndSeek.Inst.ance()
    private val config = pl.config
    private val seeker = Bukkit.getPlayer(UUID.fromString(config.get("status.seeker").toString()))

    @EventHandler
    fun onProjectileHit(e: ProjectileHitEvent) {
        if (e.entity.type == EntityType.ARROW) {
            if (e.hitEntity != null) {
                if (e.hitEntity is Player) {
                    val p = e.hitEntity as Player
                    p.addPotionEffect(PotionEffect(PotionEffectType.BLINDNESS, 60, 1), true)
                }
            }
        }
    }
}