package org.bitbucket.parangee.hideandseek.events

import javafx.concurrent.Task
import org.bitbucket.parangee.hideandseek.commands.TaskInstance
import org.bitbucket.parangee.hideandseek.plugin.HideAndSeek
import org.bitbucket.parangee.hideandseek.util.color
import org.bukkit.Bukkit
import org.bukkit.GameMode
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.PlayerDeathEvent

class DeathListener:Listener {
    private val pl = HideAndSeek.Inst.ance()
    private val config = pl.config
    @EventHandler
    fun onDeath(e:PlayerDeathEvent) {
        if (config.getString("status.seeker").toString() == e.entity.uniqueId.toString()
                &&TaskInstance.getInstance().started) {
            e.isCancelled = true
            return
        }
        if (e.entity.killer != null && e.entity.killer!!.uniqueId.toString() ==
                config.getString("status.seeker")
                && TaskInstance.players.contains(e.entity)
                && TaskInstance.getInstance().started) {
            e.isCancelled = true
            e.entity.gameMode = GameMode.SPECTATOR
            TaskInstance.players.remove(e.entity)
            Bukkit.broadcastMessage("&c[&a술래잡기&c] &f플레이어 ${e.entity.name}님이 잡혔습니다.".color())
            if (TaskInstance.players.size == 1) {
                Bukkit.getOnlinePlayers().forEach {
                    it.sendTitle("&c게임 끝!".color(), "")
                }
                TaskInstance.getInstance().stop()
            }
        }
    }
}