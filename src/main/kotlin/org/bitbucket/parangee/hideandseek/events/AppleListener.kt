package org.bitbucket.parangee.hideandseek.events

import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.player.PlayerInteractEvent

class AppleListener : Listener {
    @EventHandler
    fun onInteract(e:PlayerInteractEvent) {
        if (e.action == Action.RIGHT_CLICK_AIR
                || e.action == Action.RIGHT_CLICK_BLOCK) {
            val item = e.item ?: return
            val p = e.player
            if (item.type == Material.APPLE) {
                p.health = p.healthScale
                p.foodLevel = 20
            }
        }
    }
}