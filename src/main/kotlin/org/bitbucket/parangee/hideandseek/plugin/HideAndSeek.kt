package org.bitbucket.parangee.hideandseek.plugin

import com.github.noonmaru.tap.command.command
import org.bitbucket.parangee.hideandseek.commands.*
import org.bitbucket.parangee.hideandseek.events.AppleListener
import org.bitbucket.parangee.hideandseek.events.ArrowListener
import org.bitbucket.parangee.hideandseek.events.DeathListener
import org.bitbucket.parangee.hideandseek.util.ShowConfig
import org.bukkit.Bukkit
import org.bukkit.plugin.java.JavaPlugin
import java.io.File

class HideAndSeek:JavaPlugin() {
    val configFile = File(dataFolder, "config.yml")
    object Inst {
        private lateinit var instance:HideAndSeek
        fun ance() : HideAndSeek {
            return instance
        }
        fun set(instance:HideAndSeek) {
            this.instance = instance
        }
    }
    override fun onEnable() {
        Inst.set(this)
        Bukkit.broadcastMessage("${Inst.ance()} enabling...")
        registerCommands()
        loadConfiguration()
        registerEvent()
    }
    private fun registerCommands() {
        command("hs") {
            help("help")
            component("rule") {
                description = "게임 설명 보기"
                RuleCommand()
            }
            component("config") {
                description = "설정 보기"
                GetConfig()
            }
            component("enableItems") {
                description = "술래잡기 아이템을 활성화합니다."
                EnableItems()
            }
            component("disableItems") {
                description = "술래잡기 아이템을 비활성화합니다."
                DisableItems()
            }
            component("setSeeker") {
                description = "술래를 설정합니다."
                SetSeeker()
            }
            component("removeSeeker") {
                description = "술래를 설정에서 제거합니다."
                RemoveSeeker()
            }
            component("start") {
                description = "게임을 시작합니다"
                StartGame()
            }
            component("stop") {
                description = "게임을 종료합니다"
                StopGame()
            }
            component("setPlayTime") {
                description = "플레이 시간을 설정합니다."
                SetPlayTime()
            }
            component("setCountingTime") {
                description = "술래가 갇혀 있는 시간을 설정합니다."
                SetCountingTime()
            }
            component("join") {
                description = "술래잡기에 참여합니다"
                JoinCommand()
            }
            component("leave") {
                description = "술래잡기 관전 명령어입니다."
                LeaveCommand()
            }
            component("setStartLocation") {
                description = "시작 위치를 설정합니다"
                SetStartLocation()
            }
            component("setJailLocation") {
                description = "감옥 위치를 설정합니다."
                SetJailLocation()
            }
            component("setSpawnLocation") {
                description = "스폰 위치를 설정합니다."
                SetSpawnLocation()
            }
        }
    }
    private fun registerEvent() {
        val manager = server.pluginManager
        manager.registerEvents(AppleListener(), this)
        manager.registerEvents(DeathListener(), this)
        manager.registerEvents(ArrowListener(), this)
    }

    private fun loadConfiguration() {
        saveDefaultConfig()
        config.load(configFile)
        Bukkit.broadcastMessage(ShowConfig().show(config))
    }
}