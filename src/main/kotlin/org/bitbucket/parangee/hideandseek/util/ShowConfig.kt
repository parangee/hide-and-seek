package org.bitbucket.parangee.hideandseek.util

import org.bitbucket.parangee.chatbuilder.api.ChatBuilder
import org.bitbucket.parangee.chatbuilder.api.ChatGroup
import org.bukkit.Bukkit
import org.bukkit.configuration.file.FileConfiguration
import java.util.*

class ShowConfig {
    fun show(config:FileConfiguration) : String {
        val builder = ChatBuilder("술래잡기 설정 정보")
        var item = ChatGroup("시스템 정보")
        item.add("아이템 활성화: ${config.getBoolean("status.enabled")}")
        builder.add(item)
        item = ChatGroup("게임 설정")
        var stat:String = if (config.get("loc.start") == null) {
            "설정되지 않음"
        } else {
            "${config.getString("loc.start")}"
        }
        item.add("시작 위치: $stat")
        stat = if (config.get("loc.jail") == null) {
            "설정되지 않음"
        } else {
            "${config.getString("loc.jail")}"
        }
        item.add("감옥 위치: $stat")
        stat = if (config.get("loc.spawn") == null) {
            "설정되지 않음"
        } else {
            "${config.getString("loc.spawn")}"
        }
        item.add("스폰 위치: $stat")
        item.add("술래: ${if (!config.getString("status.seeker").isNullOrEmpty()) {
            Bukkit.getPlayer(UUID.fromString(config.getString("status.seeker")))?.name
        } else {"설정되지 않음"}}")
        item.add("카운트다운 시간: ${TimeToString.get(config.getInt("settings.countTime"))}")
        item.add("플레이 시간: ${TimeToString.get(config.getInt("settings.time"))}")
        builder.add(item)
        return builder.getResult()
    }
}