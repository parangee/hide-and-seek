package org.bitbucket.parangee.hideandseek.util

object TimeToString {
    fun get(time:Int) : String {
        val min = time/60
        val sec = time-(min*60)
        val strMin = placeZeroIfNeed(min)
        val strSec = placeZeroIfNeed(sec)
        return String.format("%s:%s", strMin, strSec)
    }
    private fun placeZeroIfNeed(number: Int): String? {
        return if (number >= 10) number.toString() else String.format("0%s", Integer.toString(number))
    }
}