package org.bitbucket.parangee.hideandseek.util

import org.bukkit.ChatColor

fun String.color() : String {
    return ChatColor.translateAlternateColorCodes('&', this)
}
