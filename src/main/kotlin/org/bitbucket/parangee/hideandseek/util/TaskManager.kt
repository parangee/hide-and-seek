package org.bitbucket.parangee.hideandseek.util

import org.bitbucket.parangee.chatbuilder.api.ChatBuilder
import org.bitbucket.parangee.chatbuilder.api.ChatGroup
import org.bitbucket.parangee.hideandseek.commands.TaskInstance
import org.bitbucket.parangee.hideandseek.plugin.HideAndSeek
import org.bukkit.Bukkit
import org.bukkit.GameMode
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.inventory.ItemStack
import java.util.*

class TaskManager {
    private val pl = HideAndSeek.Inst.ance()
    private val config = pl.config
    private var timer = pl.config.getInt("settings.countTime")
    private var counting = true
    private var waitTimer = 30
    var started = false
    var waiting = true
    fun start():Boolean {
        return if (started) {
            false
        } else {
            Bukkit.getScheduler().cancelTasks(pl)
            waiting = true
            started = true
            val builder = ChatBuilder("게임 시작")
            val group = ChatGroup("참여하시려면 30초 안에 /hs join을 입력해주세요.")
            builder.add(group)
            Bukkit.broadcastMessage(builder.getResult())
            Bukkit.getScheduler().scheduleSyncRepeatingTask(pl, {
                Bukkit.getOnlinePlayers().forEach {
                    it.sendActionBar("게임 시작까지 ${waitTimer}초")
                }
                waitTimer -= 1
            }, 0, 20)
            Bukkit.getScheduler().scheduleSyncDelayedTask(pl, {
                waiting = false
                Bukkit.getScheduler().cancelTasks(pl)
                Bukkit.broadcastMessage("&a게임 시작!".color())
                initPlayers()
                startTimer()
            }, 600)
            true
        }
    }
    private fun initPlayers() {
        val players = TaskInstance.players
        val random = Random().nextInt(players.size)
        val seeker = players[random]
        config.set("status.seeker", seeker.uniqueId.toString())
        config.save(pl.configFile)
        Bukkit.broadcastMessage("술래는 ${seeker.name}님 입니다!")
        Bukkit.getOnlinePlayers().forEach {
            it.walkSpeed = 0.25f
            it.activePotionEffects.forEach {potion ->
                it.removePotionEffect(potion.type)
            }
            it.inventory.clear()
            it.health = it.healthScale
            var loc = config.getString("loc.start")?.split("|")
            it.teleport(
                    Location(
                            Bukkit.getWorld(loc?.get(0).toString()),
                            java.lang.Double.parseDouble(loc?.get(1)),
                            java.lang.Double.parseDouble(loc?.get(2)),
                            java.lang.Double.parseDouble(loc?.get(3))
                    )
            )
            if (players.contains(it)) {
                it.gameMode = GameMode.ADVENTURE
            } else {
                it.gameMode = GameMode.SPECTATOR
            }
            if (seeker == it) {
                it.inventory.addItem(ItemStack(Material.DIAMOND_SWORD))
                val loc = config.getString("loc.jail")?.split("|")
                it.teleport(
                        Location(
                                Bukkit.getWorld(loc?.get(0).toString()),
                                java.lang.Double.parseDouble(loc?.get(1)),
                                java.lang.Double.parseDouble(loc?.get(2)),
                                java.lang.Double.parseDouble(loc?.get(3))
                        )
                )
                it.walkSpeed = 0.5f
            } else if(players.contains(it)) {
                it.inventory.addItem(ItemStack(Material.APPLE, 1))
                it.inventory.addItem(ItemStack(Material.BOW, 1))
                it.inventory.addItem(ItemStack(Material.ARROW, 3))
            }
        }
    }
    fun stop():Boolean {
        return if (!started) {
            false
        } else {
            started = false
            stopTimer()
            reset()
            true
        }
    }
    private fun reset() {
        val loc = config.getString("loc.spawn")?.split("|")
        timer = pl.config.getInt("settings.countTime")
        waitTimer = 30
        TaskInstance.players.clear()
        Bukkit.getOnlinePlayers().forEach {
            it.teleport(
                    Location(
                            Bukkit.getWorld(loc?.get(0).toString()),
                            java.lang.Double.parseDouble(loc?.get(1)),
                            java.lang.Double.parseDouble(loc?.get(2)),
                            java.lang.Double.parseDouble(loc?.get(3))
                    )
            )
            it.inventory.clear()
            it.gameMode = GameMode.ADVENTURE
            it.activePotionEffects.forEach { potion ->
                it.removePotionEffect(potion.type)
            }
            it.walkSpeed = 0.25f
        }
    }
    private fun startTimer() {
        val sch = Bukkit.getScheduler()
        sch.scheduleSyncRepeatingTask(pl, {
            if (timer <= 0 && counting) {
                timer = pl.config.getInt("settings.time")
                counting = false
                val loc = config.getString("loc.start")?.split("|")
                Bukkit.getPlayer(UUID.fromString(config.getString("status.seeker")))?.teleport(
                        Location(
                                Bukkit.getWorld(loc?.get(0).toString()),
                                java.lang.Double.parseDouble(loc?.get(1)),
                                java.lang.Double.parseDouble(loc?.get(2)),
                                java.lang.Double.parseDouble(loc?.get(3))
                        )
                )
                Bukkit.broadcastMessage("&c술래가 풀려났습니다!".color())
            } else if (timer <= 0 && !counting) {
                Bukkit.getScheduler().cancelTasks(pl)
                timer = pl.config.getInt("settings.startTime")
                counting = true
                Bukkit.getOnlinePlayers().forEach {
                    it.sendTitle("&a게임 끝!".color(), "")
                }
                stop()
            }
            timer -= 1
            Bukkit.getOnlinePlayers().forEach {
                it.sendActionBar("${if (counting) {"술래가 풀려나기까지"} else {"남은 시간"} }: " +
                        TimeToString.get(timer) + " 생존자 수: ${TaskInstance.players.size - 1}")
            }
        }, 0, 20)
    }
    private fun stopTimer() {
        val sch = Bukkit.getScheduler()
        sch.cancelTasks(pl)
    }
}